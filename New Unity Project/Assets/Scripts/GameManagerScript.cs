﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {

	public float levelStartDelay = 2f;
	public static GameManagerScript instance = null;
	public BoardManagerScript boardScript;
	public int playerFoodPoints = 100;
	public float turnDelay = .1f;
	[HideInInspector] public bool playersTurn = true;
	private Text finalHighScoreText;
	public int level = 1;
	private List<Enemy> enemies;
	private bool enemiesMoving;
	private Text levelText;
	private GameObject levelImage;
	private bool doingSetup;

	// Use this for initialization
	void Awake () 
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
		enemies = new List<Enemy>();
		DontDestroyOnLoad(gameObject);
		boardScript = GetComponent<BoardManagerScript> ();
		InitGame();
	}

    private void OnLevelWasLoaded(int index)
    {
        InitGame();

        level++;
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    void InitGame()
	{
        doingSetup = true;
        levelImage = GameObject.Find("levelImage");
		//finalHighScoreText= GameObject.Find("finalHighScoreText").GetComponent<Text>;
        levelText = GameObject.Find("levelText").GetComponent<Text>();
        levelText.text = "Day " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);

        enemies.Clear ();
		boardScript.SetupScene (level);
	}

	public void GameOver()
	{
        levelImage.SetActive(true);
        levelText.text = "After " + level + " days, you starved.";
		//finalHighScoreText = "You have previously fed" + "0" + "people with your leftover food."; will add back in when this works
		enabled = false;
    }
	public void AddEnemyToList (Enemy script)
	{
        enemies.Add(script);
	}

	IEnumerator MoveEnemies()
	{
		enemiesMoving = true;
		yield return new WaitForSeconds (turnDelay);
		if (enemies.Count == 0) 
		{
			yield return new WaitForSeconds (turnDelay);
		}

		for (int i = 0; i < enemies.Count; i++) 
		{
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds (enemies[i].moveTime);
		}
        playersTurn = true;
        enemiesMoving = false;
    }

	// Update is called once per frame
	void Update () 
	{
		if (playersTurn || enemiesMoving|| doingSetup)
			return;

		StartCoroutine (MoveEnemies ());

	}
}
