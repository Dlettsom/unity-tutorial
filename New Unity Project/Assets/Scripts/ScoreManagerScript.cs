﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreManagerScript : MonoBehaviour {
	public static int score;

	public static int highScore;
	public int highScoreProperty {
		get { 
			return highScore;
		}
		set {
			highScore = value;
			text.text = "Score: " + value;
		}

	}
	private static ScoreManagerScript _instance;
	public static ScoreManagerScript instance {
		get {
			return _instance;
		}

	}

	Text text;

	void Start()
	{
		_instance = this;

		text = GetComponent<Text> ();

		score = 0;

		highScore = PlayerPrefs.GetInt ("highscore", highScore);
	}



	/*
	void Update()
	{
		if (score > highScore);
		highScore = score;
		text.text = "" + score;

		PlayerPrefs.SetInt ("highscore", highScore);
	}

	public static void AddPoints (int pointsToAdd)
	{
		score += pointsToAdd;
	}

	public static void Reset()
	{
		score = 0;
	}
*/
}
