﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject {

	public int wallDamage = 1;
    public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public float restartLevelDelay = 1f;
    public Text foodText;
	//public static int Score;
	//public static int highScore;
	public Text scoreText;
	public Text highScoreText;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;

    private  Animator animator;
	private int food;


	// Use this for initialization
	protected override void Start () 
	{
		animator = GetComponent<Animator> ();

		food = GameManagerScript.instance.playerFoodPoints;

		base.Start ();
	}

	private void OnDisable()
	{
		GameManagerScript.instance.playerFoodPoints = food;
	}



	private void OnTriggerEnter2D (Collider2D other)
	{
        
        if (other.tag == "Exit")
		{
			Invoke ("Restart", restartLevelDelay);
			enabled = false;
		}
		else if (other.tag == "Food")
		{
			ScoreManagerScript.instance.highScoreProperty += pointsPerFood;
			food += pointsPerFood;
			// scoreText.text = "+" + pointsPerFood + " Food: " + food;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            other.gameObject.SetActive(false);
            SoundManagerScript.instance.RandomizeSfx(eatSound1, eatSound2);
        }

		else if (other.tag == "Soda")
		{
            ScoreManagerScript.instance.highScoreProperty += pointsPerFood;
            food += pointsPerSoda;
			// scoreText.text = "+" + pointsPerSoda + " Food: " + food;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            other.gameObject.SetActive(false);
            SoundManagerScript.instance.RandomizeSfx(drinkSound1, drinkSound2);
        }
	}

	protected override void OnCantMove <T> (T component)
	{
		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger ("playerChop");
	}

	private void Restart()
	{
		SceneManager.LoadScene(0);
	}

	public void LoseFood (int loss)
	{
		animator.SetTrigger ("playerHit");
		food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver();
	}

    // Update is called once per frame
    void Update()
    {
        if (!GameManagerScript.instance.playersTurn) return;

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
            vertical = 0;

        if (horizontal != 0 || vertical != 0)
            AttemptMove<Wall>(horizontal, vertical);


    }

    protected override void AttemptMove <T> (int xDir, int yDir)
	{
		food--;
        foodText.text = "Food: " + food;
        base.AttemptMove <T> (xDir, yDir);
		RaycastHit2D hit;
        if (Move (xDir, yDir, out hit))
        {
            SoundManagerScript.instance.RandomizeSfx(moveSound1, moveSound2);
        }
		CheckIfGameOver();
		GameManagerScript.instance.playersTurn = false;
	}

	private void CheckIfGameOver()
	{
		if (food <= 0)
        {
            SoundManagerScript.instance.RandomizeSfx(gameOverSound);
            SoundManagerScript.instance.musicSource.Stop();
            GameManagerScript.instance.GameOver();
        }	
	}
}
